import 'package:flutter/material.dart';

class StandardField extends StatelessWidget {
  const StandardField({
    Key? key,
    required this.alignment,
    required this.title,
  }) : super(key: key);
  final Alignment alignment;
  final String title;

  StandardField copyWith({
    Alignment? alignment,
    String? title,
  }) {
    return StandardField(
      alignment: alignment ?? this.alignment,
      title: title ?? this.title,
    );
  }

  StandardField clone() => copyWith();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: alignment,
      child: Text(
        title,
        style: Theme.of(context).textTheme.subtitle1,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
