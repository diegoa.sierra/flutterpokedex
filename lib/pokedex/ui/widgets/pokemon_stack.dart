import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:firstapp/pokedex/ui/animation/fade_indexed_stack.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PokemonStack extends StatelessWidget {
  final List<Widget> children;

  const PokemonStack({Key? key, required this.children}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _bloc = Provider.of<PokedexBloc>(context);
    return Expanded(
      child: ValueListenableBuilder<int>(
        valueListenable: _bloc.getIndexPage,
        builder: (context, indexPage, child) {
          return FadeIndexedStack(
            index: indexPage,
            children: children,
          );
        },
      ),
    );
  }
}
