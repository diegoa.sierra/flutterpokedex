import 'package:firstapp/pokedex/domain/entities/pokemon.dart';
import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'pokemon_title.dart';
import 'standard_cell_landscape.dart';

class PokemonInfoLandscape extends StatelessWidget {
  const PokemonInfoLandscape({Key? key, required this.pokemon})
      : super(key: key);
  final Pokemon pokemon;

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    final _bloc = Provider.of<PokedexBloc>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: _size.width * 0.1,
        vertical: _size.height * 0.05,
      ),
      color: pokemon.color,
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                PokemonTitle(
                  title: pokemon.name,
                ),
                SizedBox(
                  height: _size.height * 0.04,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    child: SvgPicture.asset(
                      pokemon.image,
                      height: double.infinity,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: _size.width * 0.05,
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
                width: 1,
                style: BorderStyle.solid,
              ),
              color: Colors.white,
              borderRadius: BorderRadius.circular(_size.height * 0.01),
            ),
            width: _size.width * 0.35,
            child: Center(
              child: Table(
                columnWidths: const <int, TableColumnWidth>{
                  0: FlexColumnWidth(),
                  1: FlexColumnWidth(),
                },
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                children: List.generate(
                  (pokemon.stadistics.length ~/ 2),
                  (index) {
                    final currentIndex = index * 2;
                    final stadistics = pokemon.stadistics;
                    return TableRow(
                      children: [
                        StandardCellLandscape(
                          items: [
                            _bloc.standardField.copyWith(
                              title: stadistics[currentIndex].name,
                              alignment: Alignment.center,
                            ),
                            Text(
                              stadistics[currentIndex].value,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ],
                        ),
                        currentIndex < stadistics.length
                            ? StandardCellLandscape(
                                items: [
                                  _bloc.standardField.copyWith(
                                    title: stadistics[currentIndex + 1].name,
                                    alignment: Alignment.center,
                                  ),
                                  Text(
                                    stadistics[currentIndex + 1].value,
                                    style:
                                        Theme.of(context).textTheme.headline6,
                                  ),
                                ],
                              )
                            : Container(),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
