import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class StandardOption extends StatelessWidget {
  final Color hoverColor;
  final Function() onTap;
  final String pathImage;

  const StandardOption({
    Key? key,
    required this.hoverColor,
    required this.onTap,
    required this.pathImage,
  }) : super(key: key);

  StandardOption copyWith({
    Color? hoverColor,
    Function()? onTap,
    String? pathImage,
  }) {
    return StandardOption(
      hoverColor: hoverColor ?? this.hoverColor,
      onTap: onTap ?? this.onTap,
      pathImage: pathImage ?? this.pathImage,
    );
  }

  StandardOption clone() => copyWith();

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTap,
      hoverColor: hoverColor,
      child: Container(
        height: _size.height * 0.1,
        width: _size.height * 0.1,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Color(0xFFDCDCDC),
          borderRadius: BorderRadius.circular(_size.height * 0.01),
        ),
        child: SvgPicture.asset(
          pathImage,
          fit: BoxFit.contain,
          color: Colors.black,
        ),
      ),
    );
  }
}
