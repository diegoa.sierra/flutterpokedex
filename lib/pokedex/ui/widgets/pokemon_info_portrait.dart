import 'package:firstapp/pokedex/domain/entities/pokemon.dart';
import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:firstapp/pokedex/ui/widgets/pokemon_title.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class PokemonInfoPortrait extends StatelessWidget {
  const PokemonInfoPortrait({Key? key, required this.pokemon})
      : super(key: key);
  final Pokemon pokemon;

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    final _bloc = Provider.of<PokedexBloc>(context);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 40,
        vertical: 30,
      ),
      color: pokemon.color,
      child: Center(
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: _size.height * 0.25,
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                        width: 1,
                        style: BorderStyle.solid,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(_size.height * 0.01),
                    ),
                    width: double.infinity,
                    padding: EdgeInsets.only(
                      top: _size.height * 0.05,
                      left: _size.height * 0.04,
                      right: _size.height * 0.04,
                    ),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisExtent: _size.height * 0.05,
                        crossAxisSpacing: _size.height * 0.05,
                      ),
                      itemCount: pokemon.stadistics.length * 2,
                      itemBuilder: (BuildContext context, int index) {
                        int currentIndex = index ~/ 2;
                        final stadistics = pokemon.stadistics;
                        if (index % 2 == 0)
                          return _bloc.standardField
                              .copyWith(title: stadistics[currentIndex].name);
                        else
                          return _bloc.standardField
                              .copyWith(title: stadistics[currentIndex].value);
                      },
                    ),
                  ),
                )
              ],
            ),
            Column(
              children: [
                PokemonTitle(
                  title: pokemon.name,
                ),
                SizedBox(
                  height: _size.height * 0.04,
                ),
                SvgPicture.asset(
                  pokemon.image,
                  height: _size.height * 0.2,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
