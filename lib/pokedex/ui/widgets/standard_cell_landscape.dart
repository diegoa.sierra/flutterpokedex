import 'package:flutter/widgets.dart';

class StandardCellLandscape extends StatelessWidget {
  final List<Widget> items;

  const StandardCellLandscape({Key? key, required this.items})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return TableCell(
      child: Container(
        height: _size.height * 0.15,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: items,
        ),
      ),
    );
  }
}
