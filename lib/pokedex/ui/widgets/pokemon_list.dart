import 'package:firstapp/pokedex/domain/entities/pokemon.dart';
import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PokemonList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    final _bloc = Provider.of<PokedexBloc>(context);
    final _elements = _bloc.getElements;
    return Container(
      height: _size.height * 0.2,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Center(
              child: FittedBox(
                fit: BoxFit.contain,
                child: _bloc.standardField.copyWith(title: 'OTHERS'),
              ),
            ),
          ),
          Row(
            children: _elements!
                .map(
                  (pokemon) => _bloc.standardItem.copyWith(
                    pathImage: pokemon.image,
                    onTap: () =>
                        _bloc.getIndexPage.value = _elements.indexOf(pokemon),
                    hoverColor: pokemon.color,
                  ),
                )
                .toList(),
          ),
          SizedBox(
            width: _size.width * 0.1,
          )
        ],
      ),
    );
  }
}
