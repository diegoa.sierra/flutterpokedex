import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PokemonTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    final _bloc = Provider.of<PokedexBloc>(context);
    final _elements = _bloc.getElements;
    return Container(
      height: _size.height * 0.3,
      color: Colors.white,
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        children: [
          Image.asset(
            'assets/pokeball.png',
            fit: BoxFit.fitHeight,
            height: _size.height * 0.1,
            alignment: Alignment.centerLeft,
            color: Colors.grey.withOpacity(0.1),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: _size.width * 0.2),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Center(
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: _bloc.standardField.copyWith(title: 'OTHERS'),
                    ),
                  ),
                ),
                Center(
                  child: Table(
                    columnWidths: const <int, TableColumnWidth>{
                      1: FlexColumnWidth(),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: List.generate(
                      2,
                      (index) {
                        final currentIndex = index * 2;
                        final leftElement = _elements![currentIndex];
                        final rightElement = _elements[currentIndex + 1];
                        return TableRow(
                          children: [
                            TableCell(
                              child: _bloc.standardItem.copyWith(
                                pathImage: leftElement.image,
                                onTap: () =>
                                    _bloc.getIndexPage.value = currentIndex,
                                hoverColor: leftElement.color,
                              ),
                            ),
                            TableCell(
                              child: _bloc.standardItem.copyWith(
                                pathImage: rightElement.image,
                                onTap: () =>
                                    _bloc.getIndexPage.value = currentIndex + 1,
                                hoverColor: rightElement.color,
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
