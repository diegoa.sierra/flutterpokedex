import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:firstapp/pokedex/presenter/state.dart';
import 'package:firstapp/pokedex/ui/widgets/pokemon_info_landscape.dart';
import 'package:firstapp/pokedex/ui/widgets/pokemon_list.dart';
import 'package:firstapp/pokedex/ui/widgets/pokemon_stack.dart';
import 'package:firstapp/pokedex/ui/widgets/pokemon_table.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'widgets/pokemon_info_portrait.dart';

class PokedexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _orientation = MediaQuery.of(context).orientation;
    final _bloc = Provider.of<PokedexBloc>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Align(
          alignment: _orientation == Orientation.portrait
              ? Alignment.center
              : Alignment.centerLeft,
          child: Text(
            'POKÉDEX_',
          ),
        ),
      ),
      body: ValueListenableBuilder(
        valueListenable: _bloc.getPokemonState,
        builder: (context, stateValue, child) {
          if (stateValue == PokemonState.Busy) {
            return Center(
              child: CircularProgressIndicator(
                color: Colors.black,
              ),
            );
          } else {
            return _orientation == Orientation.portrait
                ? Center(
                    child: Container(
                      constraints: const BoxConstraints.expand(),
                      child: Column(
                        children: [
                          PokemonStack(
                            children: _bloc.getElements!
                                .map((pokemon) => PokemonInfoPortrait(
                                      pokemon: pokemon,
                                    ))
                                .toList(),
                          ),
                          PokemonTable(),
                        ],
                      ),
                    ),
                  )
                : Center(
                    child: Container(
                      constraints: const BoxConstraints.expand(),
                      child: Column(
                        children: [
                          PokemonStack(
                            children: _bloc.getElements!
                                .map((pokemon) => PokemonInfoLandscape(
                                      pokemon: pokemon,
                                    ))
                                .toList(),
                          ),
                          PokemonList(),
                        ],
                      ),
                    ),
                  );
          }
        },
      ),
    );
  }
}
