import 'package:firstapp/pokedex/data/models/pokedex.dart';
import 'package:firstapp/pokedex/domain/pokedex_repository.dart';

abstract class GeneralPokedex {
  Future<Pokedex> readInfo();
}

class GeneralPokedexImpl implements GeneralPokedex {
  GeneralPokedexImpl(this._pokedexRepository);
  final PokedexRepository _pokedexRepository;

  @override
  Future<Pokedex> readInfo() async {
    return await _pokedexRepository.getInfo('assets/pokemon.json');
  }
}
