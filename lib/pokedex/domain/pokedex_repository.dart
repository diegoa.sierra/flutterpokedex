import 'package:firstapp/pokedex/data/models/pokedex.dart';

abstract class PokedexRepository {
  Future<Pokedex> getInfo(String path);
}
