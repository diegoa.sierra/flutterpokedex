import 'dart:ui';

import 'package:firstapp/pokedex/data/models/electric_pokemon.dart';
import 'package:firstapp/pokedex/data/models/feature.dart';
import 'package:firstapp/pokedex/data/models/fire_pokemon.dart';
import 'package:firstapp/pokedex/data/models/grass_pokemon.dart';
import 'package:firstapp/pokedex/data/models/water_pokemon.dart';

abstract class Pokemon {
  final String name;
  final String image;
  final List<Feature> stadistics;
  final Color color;

  Pokemon(
    this.name,
    this.stadistics,
    this.color,
    this.image,
  );

  factory Pokemon.fromJson(Map<String, dynamic> json) {
    final String type = json['stadistics']['TYPE'];
    final Map<String, Pokemon> pokemonOptions = {
      'fire': FirePokemon.fromJson(json),
      'water': WaterPokemon.fromJson(json),
      'grass': GrassPokemon.fromJson(json),
      'electric': ElectricPokemon.fromJson(json),
    };
    return pokemonOptions[type] ?? ElectricPokemon.fromJson(json);
  }

  String get getPathType;
}
