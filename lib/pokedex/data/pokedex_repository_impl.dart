import 'dart:convert';

import 'package:firstapp/pokedex/data/models/pokedex.dart';
import 'package:firstapp/pokedex/domain/pokedex_repository.dart';
import 'package:flutter/services.dart';

class PokedexRepositoryImpl implements PokedexRepository {
  @override
  Future<Pokedex> getInfo(String path) async {
    final String info = await rootBundle.loadString('assets/pokemon.json');
    final Map<String, dynamic> jsonInfo = jsonDecode(info);
    return Pokedex.fromJson(jsonInfo);
  }
}
