import 'package:firstapp/pokedex/domain/entities/pokemon.dart';

class Pokedex {
  final List<Pokemon> elements;

  Pokedex(this.elements);

  factory Pokedex.fromJson(Map<String, dynamic> json) =>
      Pokedex((json['data'] as List)
          .map(
            (e) => Pokemon.fromJson(e),
          )
          .toList());
}
