import 'dart:ui';

import 'package:firstapp/pokedex/domain/entities/pokemon.dart';

import 'feature.dart';

class WaterPokemon extends Pokemon {
  final String name;
  final List<Feature> stadistics;
  final Color color;
  final String image;

  WaterPokemon(this.name, this.stadistics, this.color, this.image)
      : super(name, stadistics, color, image);

  factory WaterPokemon.fromJson(Map<String, dynamic> json) {
    return WaterPokemon(
      json['name'],
      (json['stadistics'] as Map)
          .entries
          .map((e) => Feature(e.key, e.value))
          .toList(),
      Color(0xFF7AB8C8),
      json['image'],
    );
  }

  @override
  String get getPathType => throw UnimplementedError();
}
