class Feature {
  final String name;
  final String value;

  Feature(this.name, this.value);
}
