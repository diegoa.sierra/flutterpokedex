import 'dart:ui';

import 'package:firstapp/pokedex/domain/entities/pokemon.dart';

import 'feature.dart';

class FirePokemon extends Pokemon {
  final String name;
  final List<Feature> stadistics;
  final Color color;
  final String image;

  FirePokemon(this.name, this.stadistics, this.color, this.image)
      : super(name, stadistics, color, image);

  factory FirePokemon.fromJson(Map<String, dynamic> json) {
    return FirePokemon(
      json['name'],
      (json['stadistics'] as Map)
          .entries
          .map((e) => Feature(e.key, e.value))
          .toList(),
      Color(0xFFFEA7A7),
      json['image'],
    );
  }

  @override
  String get getPathType => throw UnimplementedError();
}
