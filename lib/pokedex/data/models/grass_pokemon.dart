import 'dart:ui';

import 'package:firstapp/pokedex/data/models/feature.dart';
import 'package:firstapp/pokedex/domain/entities/pokemon.dart';

class GrassPokemon extends Pokemon {
  final String name;
  final List<Feature> stadistics;
  final Color color;
  final String image;

  GrassPokemon(this.name, this.stadistics, this.color, this.image)
      : super(name, stadistics, color, image);

  factory GrassPokemon.fromJson(Map<String, dynamic> json) {
    return GrassPokemon(
      json['name'],
      (json['stadistics'] as Map)
          .entries
          .map((e) => Feature(e.key, e.value))
          .toList(),
      Color(0xFF92E3D0),
      json['image'],
    );
  }

  @override
  String get getPathType => throw UnimplementedError();
}
