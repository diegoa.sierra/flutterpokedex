import 'dart:ui';

import 'package:firstapp/pokedex/domain/entities/pokemon.dart';

import 'feature.dart';

class ElectricPokemon extends Pokemon {
  final String name;
  final List<Feature> stadistics;
  final Color color;
  final String image;

  ElectricPokemon(this.name, this.stadistics, this.color, this.image)
      : super(name, stadistics, color, image);

  factory ElectricPokemon.fromJson(Map<String, dynamic> json) {
    return ElectricPokemon(
      json['name'],
      (json['stadistics'] as Map)
          .entries
          .map((e) => Feature(e.key, e.value))
          .toList(),
      Color(0xFFFECF2F),
      json['image'],
    );
  }

  @override
  String get getPathType => throw UnimplementedError();
}
