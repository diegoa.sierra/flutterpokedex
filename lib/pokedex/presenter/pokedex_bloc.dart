import 'package:firstapp/pokedex/domain/entities/pokemon.dart';
import 'package:firstapp/pokedex/domain/usecases/general_pokedex.dart';
import 'package:firstapp/pokedex/presenter/state.dart';
import 'package:firstapp/pokedex/ui/widgets/standard_field.dart';
import 'package:firstapp/pokedex/ui/widgets/standard_option.dart';
import 'package:flutter/material.dart';

class PokedexBloc {
  final GeneralPokedex _generalPokedex;

  PokedexBloc(this._generalPokedex);

  StandardField standardField = StandardField(
    alignment: Alignment.centerLeft,
    title: 'NO.',
  );

  StandardOption standardItem = StandardOption(
    hoverColor: Colors.transparent,
    onTap: () {},
    pathImage: 'assets/charmander.svg',
  );

  final ValueNotifier<int> _indexPage = ValueNotifier<int>(0);
  final ValueNotifier<PokemonState> _state =
      ValueNotifier<PokemonState>(PokemonState.Busy);
  List<Pokemon>? _elements;

  ValueNotifier<int> get getIndexPage => _indexPage;
  ValueNotifier<PokemonState> get getPokemonState => _state;
  List<Pokemon>? get getElements => _elements;

  set setElements(List<Pokemon> elements) => _elements = elements;

  Future<void> loadInfo() async {
    final response = await _generalPokedex.readInfo();
    setElements = response.elements;
    getPokemonState.value = PokemonState.Idle;
  }
}
