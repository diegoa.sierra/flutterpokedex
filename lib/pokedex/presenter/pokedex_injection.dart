import 'package:firstapp/pokedex/data/pokedex_repository_impl.dart';
import 'package:firstapp/pokedex/domain/pokedex_repository.dart';
import 'package:firstapp/pokedex/domain/usecases/general_pokedex.dart';
import 'package:firstapp/pokedex/presenter/pokedex_bloc.dart';
import 'package:firstapp/pokedex/ui/pokedex_page.dart';
import 'package:provider/provider.dart';

class PokedexInjection {
  PokedexInjection._internal();

  static PokedexInjection? _instance;
  static PokedexInjection? get instance {
    if (_instance == null) {
      _instance = PokedexInjection._internal();
    }
    return _instance;
  }

  final _injection = MultiProvider(
    providers: [
      Provider<PokedexRepository>(
        create: (context) => PokedexRepositoryImpl(),
      ),
      Provider<GeneralPokedex>(
        create: (context) => GeneralPokedexImpl(
          context.read<PokedexRepository>(),
        ),
      ),
      Provider<PokedexBloc>(
        create: (context) => PokedexBloc(
          context.read<GeneralPokedex>(),
        )..loadInfo(),
      ),
    ],
    child: PokedexPage(),
  );

  MultiProvider get getInjection => _injection;
}
